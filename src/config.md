+++
author = "Laurent Fainsin"
mintoclevel = 2

ignore = ["node_modules/", "_data/"]

generate_sitemap = true
generate_rss = false
+++

\newcommand{\R}{\mathbb R}
\newcommand{\scal}[1]{\langle #1 \rangle}
