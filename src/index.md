@def title = "Laurent Fainsin"

Hi 👋, I'm Laurent.
I'm currently an M2 [engineering](https://www.francecompetences.fr/recherche/rncp/35713/) student at [ENSEEIHT](https://www.enseeiht.fr/) in France.
I study [Computer Science](https://www.enseeiht.fr/fr/formation/formation-ingenieur/departement-sn/programme-sn.html) and this is my personnal website to showcase my work.
I'm interested in machine learning and computer graphics.
I'm also very active in the associative life of my school.

Here is my [resume](/assets/resume.pdf) if you are professionally interested.

You can email me at:

- [laurent@~~~<WBR>~~~fainsin.bzh](mailto:laurent@fainsin.bzh)
- [laurentfainsin@~~~<WBR>~~~protonmail.com](mailto:laurentfainsin@protonmail.com) ([PGP](/assets/public.pgp))
- [laurent.fainsin@~~~<WBR>~~~etu.inp-n7.fr](mailto:laurent.fainsin@etu.inp-n7.fr)

You can reach me via:

- [GitHub](https://github.com/Laurent2916)
- [GitLab (INP-net)](https://git.inpt.fr/fainsil)
- [LinkedIn](https://www.linkedin.com/in/laurent-fainsin/)
- [[Matrix]](https://matrix.to/#/@fainsil:inpt.fr)
