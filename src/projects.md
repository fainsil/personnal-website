@def title = "Projects"

Here is a non-exhaustive list of my projects. \
Most of them are in French 🇫🇷 🥖 🥐.

# Professional projects

{{ print_projects _data/professional_projects.yml }}

# Personnal projects

{{ print_projects _data/personnal_projects.yml }}

# School projects

{{ print_projects _data/school_projects.yml }}
