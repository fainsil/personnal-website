using YAML
using OrderedCollections
using Franklin

function hfun_print_projects(yaml_files)
  yaml = YAML.load_file(yaml_files[1]; dicttype=OrderedDict{String,Any})

  html_string = ""
  for (project_name, project) in yaml
    description = Franklin.fd2html(project["description"], internal=true)
    repository = haskey(project, "repository") ? """<a href="$(project["repository"])">💾</a>""" : ""
    website = haskey(project, "website") ? """<a href="$(project["website"])">🌐</a>""" : ""
    pdf = haskey(project, "pdf") ? """<a href="$(project["pdf"])">📜</a>""" : ""
    year = """<span class="year">$(project["year"])</span>"""
    image = haskey(project, "image") ? """<img src="$(project["image"])">""" : ""
    video = haskey(project, "video") ? """<video controls autoplay loop><source src="$(project["video"])"></video>""" : ""

    html_string = """
    $(html_string)
      <h2>$(project_name), <small>$(year)</small></h2>
      $(video)
      $(image)
      $(description)
      <span class="icons">
      $(repository)
      $(pdf)
      $(website)
      </span>
    """
  end

  return html_string
end
